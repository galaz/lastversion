<?php

use yii\db\Migration;

class m170612_124020_alter_user_and_lead_tables extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
    }

    public function down()
    {
        $this->dropColumn('user','firstname');
		$this->dropColumn('user','lastname');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
