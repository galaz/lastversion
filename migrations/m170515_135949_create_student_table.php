<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170515_135949_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey()
        ]);
		
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
