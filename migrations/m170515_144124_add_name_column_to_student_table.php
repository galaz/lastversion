<?php

use yii\db\Migration;

/**
 * Handles adding name to table `student`.
 */
class m170515_144124_add_name_column_to_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('student', 'name', $this->string(12));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('student', 'name');
    }
}
