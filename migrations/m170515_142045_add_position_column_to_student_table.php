<?php

use yii\db\Migration;

/**
 * Handles adding position to table `student`.
 */
class m170515_142045_add_position_column_to_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('student', 'position', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('student', 'position');
    }
}
