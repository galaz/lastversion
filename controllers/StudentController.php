<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller// הקונטרולר היא מחלקה מקורית של yii 
{
	  public function actionView($id)
    {
		$name =  Student::getName($id);
        return $this->render('view', ['name' => $name]);
    }
}
